/* MENU CONF*/
(function ($) {
    "use strict";


    $("#owl-demo").owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds
        dots: true,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 4]
    });
    $("#owl-team").owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds
        dots: true,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 4]
    });
    $("#owl-partners").owlCarousel({

        autoPlay: 3000, //Set AutoPlay to 3 seconds
        dots: true,
        items: 5,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 4]
    });



$("#owl").owlCarousel({
    navigation : true, 
slideSpeed : 300,
paginationSpeed : 400,
singleItem: true,
 pagination: false,
rewindSpeed: 500,
 dots: false
, navText: ['&#xeb32;'
     , '&#xeb33;'
 ]
 , items: 3
});






var num = 50; //number of pixels before modifying styles
        if ($(window).scrollTop() > num) {
            $('nav').addClass('scrolled');
        }
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
                $('nav').addClass('scrolled');

            } else {
                $('nav').removeClass('scrolled');
            }
        });
/*
        $('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
  */      
 var showChar = 150;  // How many characters are shown by default
 var ellipsestext = "...";
 var moretext = "Show more ...";
 var lesstext = "Show less";
 

 $('.more').each(function() {
     var content = $(this).html();

     if(content.length > showChar) {

         var c = content.substr(0, showChar);
         var h = content.substr(showChar, content.length - showChar);

         var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

         $(this).html(html);
     }

 });

 $(".morelink").click(function(){
     if($(this).hasClass("less")) {
         $(this).removeClass("less");
         $(this).html(moretext);
     } else {
         $(this).addClass("less");
         $(this).html(lesstext);
     }
     $(this).parent().prev().toggle();
     $(this).prev().toggle();
     return false;
 });
})(jQuery);
